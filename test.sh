mkdir reports
go test -cover -v 2>&1 | go-junit-report > reports/report.xml
go test -coverprofile=reports/coverage.out
go tool cover -html=reports/coverage.out -o reports/debug_coverage.html
gocov test | gocov-html > reports/coverage.html
goreporter -p . -r reports -e vendor/ -f [json/html]