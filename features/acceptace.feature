# file: $GOPATH/src/golangweb/acceptance/acceptance.feature
Feature: Acceptance
  In order to check app functionality
  github repos should be listed when provide a proper url

  Scenario: user test
    Given github username
    When server up
    Then repositories should be listed
  
  Scenario: url test
    Given invalid url
    When servers up
    Then show not found (400) error
  
  Scenario: other test
    Given something
    When some condition
    Then something happen