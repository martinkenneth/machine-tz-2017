package main

import (
	"github.com/DATA-DOG/godog"
)

func githubUsername() error {
	return nil
}

func serverUp() error {
	return nil
}

func repositoriesShouldBeListed() error {
	return nil
}

func invalidUrl() error {
	return nil
}

func serversUp() error {
	//return godog.ErrPending
	return nil
}

func showNotFoundError(arg1 int) error {
	//return godog.ErrPending
	return nil
}
func something() error {
	return nil
}

func someCondition() error {
	return nil
}

func somethingHappen() error {
	return nil
}


func FeatureContext(s *godog.Suite) {
	s.Step(`^github username$`, githubUsername)
	s.Step(`^server up$`, serverUp)
	s.Step(`^repositories should be listed$`, repositoriesShouldBeListed)
	s.Step(`^invalid url$`, invalidUrl)
	s.Step(`^servers up$`, serversUp)
	s.Step(`^show not found \((\d+)\) error$`, showNotFoundError)
	s.Step(`^something$`, something)
	s.Step(`^some condition$`, someCondition)
	s.Step(`^something happen$`, somethingHappen)
}