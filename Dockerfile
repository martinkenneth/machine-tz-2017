FROM golang:latest
RUN go get github.com/Masterminds/glide && go get github.com/axw/gocov/gocov && go get github.com/jstemmer/go-junit-report && go get -u gopkg.in/matm/v1/gocov-html && go get -u github.com/wgliang/goreporter && go get github.com/DATA-DOG/godog/cmd/godog
RUN apt update && apt-get install graphviz -y
ADD golangweb.tar.gz /go/src/golangweb
WORKDIR /go/src/golangweb
RUN glide up && chmod +x test.sh 
# docker build -t golangweb .
# docker run -d -p 8080:8080 golangweb 
